﻿using UnityEngine;

public class Mover : MonoBehaviour
{
    // skin width, for making sure we don't cast through the floor on a precise put

    private static readonly float Gravity = 1f;
    private static readonly float TerminalVelocity = 1f;

    public float height = 1f;
    public float width = .5f;
    public float SkinWidth = .2f;
    public float horizontalSpeed = 1f;
    public float horizontalDistance = 0.04f;

    private float verticalMomentum;

    private void Update()
    {
        UpdateVerticalMovement();
        UpdateHorizontalMovement();

        DebugRespawnCheck(); //Debug
    }

    private Vector3 GetBoxCenter() { return transform.position + Vector3.up * height * .5f; }
    private Vector3 GetHalfExtends() { return (new Vector3(width, height, width)) * .5f; }

    private Vector3 GetBoxCastCenter() { return GetBoxCenter() + Vector3.up * SkinWidth * .5f; }
    private Vector3 GetBoxCastHalfExtends() { return GetHalfExtends() - Vector3.up * SkinWidth * .5f; }

    private void UpdateVerticalMovement()
    {
        verticalMomentum -= (Gravity * Time.deltaTime);
        verticalMomentum = Mathf.Clamp(verticalMomentum, -TerminalVelocity, TerminalVelocity);

        Vector3 boxCastCenter = GetBoxCastCenter();
        Vector3 boxCastHalfExtends = GetBoxCastHalfExtends();

        RaycastHit info;
        bool hit = Physics.BoxCast(
            boxCastCenter,
            boxCastHalfExtends,
            Vector3.down,
            out info);

        float distanceFromBox = info.distance - SkinWidth;
        DebugVerticalHit(hit, info.point, boxCastCenter, boxCastHalfExtends, distanceFromBox); //Debug

        if (hit && distanceFromBox < Mathf.Abs(verticalMomentum))
        {
            transform.position += Vector3.down * (info.distance - SkinWidth);
            verticalMomentum = 0f;
        }
        else
            transform.position += Vector3.up * verticalMomentum;
    }

    private void UpdateHorizontalMovement()
    {
        Vector3 translation =
            Vector3.right * Mathf.Sin(Time.time * horizontalSpeed) * horizontalDistance +
            Vector3.forward * Mathf.Cos(Time.time * horizontalSpeed) * horizontalDistance;

        Vector3 direction = Vector3.Normalize(translation);

        Vector3 boxCastCenter = GetBoxCastCenter();
        Vector3 boxCastHalfExtends = GetBoxCastHalfExtends();

        RaycastHit info;
        bool hit = Physics.BoxCast(
            boxCastCenter,
            boxCastHalfExtends,
            direction,
            out info);

        DebugHorizontalHit(info.point); //Debug

        if (!hit)
            Debug.Log("No Hit");
        else if (info.distance < translation.magnitude)
            Debug.Log("Too Close");
        else
            transform.position += translation;

    }

    #region debug
    private Vector3 _debugPosition;
    private Quaternion _debugRotation;
    
    private void Awake()
    {
        _debugPosition = transform.position;
        _debugRotation = transform.rotation;
    }

    private void DebugRespawnCheck()
    {
        if (transform.position.y >= -5)
            return;

        transform.position = _debugPosition;
        transform.rotation = _debugRotation;
        verticalMomentum = 0f;
    }

    private void DebugVerticalHit(bool didhit, Vector3 hitpoint, Vector3 boxCastCenter, Vector3 boxCastHalfExtends, float distanceFromBox)
    {
        Debug.DrawLine(boxCastCenter, boxCastCenter + boxCastHalfExtends, Color.red);
        Debug.DrawLine(boxCastCenter, boxCastCenter - boxCastHalfExtends, Color.yellow);
        Debug.DrawLine(hitpoint, hitpoint + Vector3.up * distanceFromBox, Color.red);

        if (!didhit)
        {
            Debug.DrawLine(
                transform.position + Vector3.right + Vector3.down,
                transform.position + Vector3.left + Vector3.up,
                Color.red);
        }
        else
        {
            Debug.DrawLine(hitpoint + Vector3.down, hitpoint + Vector3.up);
            Debug.DrawLine(hitpoint + Vector3.left, hitpoint + Vector3.right);
            Debug.DrawLine(hitpoint + Vector3.forward, hitpoint + Vector3.back);
        }
    }

    private void DebugHorizontalHit(Vector3 hitpoint)
    {
        Debug.DrawLine(hitpoint + Vector3.down, hitpoint + Vector3.up);
        Debug.DrawLine(hitpoint + Vector3.left, hitpoint + Vector3.right);
        Debug.DrawLine(hitpoint + Vector3.forward, hitpoint + Vector3.back);
    }

    public void OnDrawGizmos()
    {
        Debug.DrawLine(transform.position, transform.position + Vector3.up * height);

        Vector3 center = transform.position + Vector3.up * height * .5f;
        Debug.DrawLine(center + Vector3.right * width * .5f, center + Vector3.left * width * .5f);
        Debug.DrawLine(center + Vector3.forward * width * .5f, center + Vector3.back * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.forward * width * .5f + Vector3.right * width * .5f,
            transform.position + Vector3.back * width * .5f + Vector3.right * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.forward * width * .5f + Vector3.right * width * .5f,
            transform.position + Vector3.forward * width * .5f + Vector3.left * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.forward * width * .5f + Vector3.left * width * .5f,
            transform.position + Vector3.back * width * .5f + Vector3.left * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.back * width * .5f + Vector3.right * width * .5f,
            transform.position + Vector3.back * width * .5f + Vector3.left * width * .5f);


        Debug.DrawLine(
            center + Vector3.forward * width * .5f + Vector3.right * width * .5f,
            center + Vector3.back * width * .5f + Vector3.right * width * .5f);

        Debug.DrawLine(
            center + Vector3.forward * width * .5f + Vector3.right * width * .5f,
            center + Vector3.forward * width * .5f + Vector3.left * width * .5f);

        Debug.DrawLine(
            center + Vector3.forward * width * .5f + Vector3.left * width * .5f,
            center + Vector3.back * width * .5f + Vector3.left * width * .5f);

        Debug.DrawLine(
            center + Vector3.back * width * .5f + Vector3.right * width * .5f,
            center + Vector3.back * width * .5f + Vector3.left * width * .5f);


        Debug.DrawLine(
           transform.position + Vector3.up * height + Vector3.forward * width * .5f + Vector3.right * width * .5f,
           transform.position + Vector3.up * height + Vector3.back * width * .5f + Vector3.right * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.up * height + Vector3.forward * width * .5f + Vector3.right * width * .5f,
            transform.position + Vector3.up * height + Vector3.forward * width * .5f + Vector3.left * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.up * height + Vector3.forward * width * .5f + Vector3.left * width * .5f,
            transform.position + Vector3.up * height + Vector3.back * width * .5f + Vector3.left * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.up * height + Vector3.back * width * .5f + Vector3.right * width * .5f,
            transform.position + Vector3.up * height + Vector3.back * width * .5f + Vector3.left * width * .5f);


        Debug.DrawLine(
          transform.position + Vector3.forward * width * .5f + Vector3.right * width * .5f,
          transform.position + Vector3.up * height + Vector3.forward * width * .5f + Vector3.right * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.forward * width * .5f + Vector3.left * width * .5f,
            transform.position + Vector3.up * height + Vector3.forward * width * .5f + Vector3.left * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.back * width * .5f + Vector3.right * width * .5f,
            transform.position + Vector3.up * height + Vector3.back * width * .5f + Vector3.right * width * .5f);

        Debug.DrawLine(
            transform.position + Vector3.back * width * .5f + Vector3.left * width * .5f,
            transform.position + Vector3.up * height + Vector3.back * width * .5f + Vector3.left * width * .5f);
    }
    #endregion
}